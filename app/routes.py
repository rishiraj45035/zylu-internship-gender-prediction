from app import appVariable
from flask import render_template, Response
from  app import genderDetector
user = {'name': 'Rishi'}
@appVariable.route('/')
def index():    
    return render_template('view.html', title='Home', user=user)
@appVariable.route('/video_feed')
def video_feed():
    return Response(genderDetector.gen(),
                    mimetype='multipart/x-mixed-replace; boundary=frame')