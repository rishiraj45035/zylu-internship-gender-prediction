import cv2
import numpy as np
#load pre-trained data 
face_proto="app/model/deploy.prototxt.txt"
face_model="app/model/res10_300x300_ssd_iter_140000.caffemodel"
gender_proto="app/model/deploy_gender.prototxt"
gender_model="app/model/gender_net.caffemodel"
gender_list=["Male","Female"]

#Constructing network for face and gender detection
face_net=cv2.dnn.readNetFromCaffe(face_proto,face_model)
gender_net = cv2.dnn.readNetFromCaffe(gender_proto,gender_model)


def detectFace(frame,minConfidence=0.6):
    '''
    Detect faces in the frame
    @param frame: frame to detect faces
    @param minConfidence: minimum confidence to detect faces
    @return: list of detected faces
    '''
    blob=cv2.dnn.blobFromImage(frame,1.0,(300,300),(104.0,177.0,123.0))
    face_net.setInput(blob)
    detections=np.squeeze(face_net.forward())
    faces=[]
    for i in range(detections.shape[0]):
        confidence=detections[i,2]
        if confidence>minConfidence:
            box=detections[i,3:7]*np.array([frame.shape[1],frame.shape[0],frame.shape[1],frame.shape[0]])
            (startX,startY,endX,endY)=box.astype("int")
            faces.append((startX,startY,endX,endY))
    return faces
def predictGender(face):
    '''
    Predict gender of the face
    @param face: face to predict

    @return  prediction
    '''
    blob=cv2.dnn.blobFromImage(face,1.0,(227,227),(78.4263377603,87.7689143744,114.895847746),swapRB=False,crop=False)
    gender_net.setInput(blob)
    detection=gender_net.forward()
    return detection


def gen():
    '''
    Generate frames for video feed along with gender prediction
    @return: frame in bytes
    '''
    
    cap = cv2.VideoCapture(0)
    while True:
        ret, frame = cap.read()
        faces=detectFace(frame)
        for i,(startX,startY,endX,endY) in enumerate(faces):
            face=frame[startY:endY,startX:endX]
            detection=predictGender(face)
            i=np.argmax(detection)
            gender=gender_list[i]
            gender_confidence=detection[0][i]
            text=f"{gender}-{gender_confidence*100:.2f}%"
            y=startY-10 if startY-10>10 else startY+10
            cv2.rectangle(frame,(startX,startY),(endX,endY),(0,255,0),2)
            cv2.putText(frame,text,(startX,y),cv2.FONT_HERSHEY_SIMPLEX,0.45,(0,255,0),2)
        ret, jpeg = cv2.imencode('.jpg', frame)
        frame = jpeg.tobytes()
        yield (b'--frame\r\n'
               b'Content-Type: image/jpeg\r\n\r\n' + (frame) + b'\r\n')
