

## Gender Predictor App
This app predicts the gender of the user using opencv DNN module.


## Description
Face is detected in the live video feed taken from the webcam(can detect multiple faces). The face is then passed as input to gender opencv neural network to predict the gender.

## Screenshots
![working of APP](https://i.imgur.com/N9SllJ0.png)

## Installation
You can either download venv folder or install all of the dependencies in requirements.txt
### using venv:
```bash
cd Zylu-internship-Gender-Prediction
source venv/bin/activate
flask run
```
### Without venv:
```bash
cd Zylu-internship-Gender-Prediction
pip install -r requirements.txt
flask run
```
#### open the local host(typically http://127.0.0.1:5000/ ) in a browser or clik on the link in terminal

## Roadmap
Add Login facility<br />
Add different filters

